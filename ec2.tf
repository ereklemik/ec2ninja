provider "aws" {
  region = "eu-central-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}


resource "aws_security_group" "sg" {
  name        = "sg"
  description = "Security group for the instance"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


output "instance_ip" {
  value = aws_instance.ec2_instance.public_ip
}


resource "tls_private_key" "ssh_key" {
  algorithm   = "RSA"
  rsa_bits    = 4096
  ecdsa_curve = "P256"
}


resource "aws_key_pair" "ec2ninjakey" {
  key_name   = "ec2ninja-key"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

resource "aws_instance" "ec2_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ec2ninjakey.key_name
  vpc_security_group_ids = [aws_security_group.sg.id]

  tags = {
    Name = "EC2Ninja"
  }
}



output "ssh_private_key" {
  value       = tls_private_key.ssh_key.private_key_pem
  description = "SSH private key"
  sensitive = true
}



















