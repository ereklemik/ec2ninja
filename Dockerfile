FROM python:3.9
WORKDIR /ec2ninja/flask_app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY flask_app/app.py .
COPY flask_app/templates ./templates
COPY flask_app/static ./static
EXPOSE 5000
CMD ["python", "app.py"]
