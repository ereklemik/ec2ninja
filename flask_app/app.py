import boto3
from flask import Flask, render_template, request, redirect
import os

app = Flask(__name__)

instance_ids = []  # List to store added instance IDs

@app.route('/')
def home():
    return render_template('index.html', instance_ids=instance_ids)

@app.route('/add_instance', methods=['POST'])
def add_instance():
    instance_id = request.form['instance_id']

    # Check if the instance ID already exists in the list
    if instance_id in instance_ids:
        message = f'Instance ID {instance_id} already exists'
    else:
        instance_ids.append(instance_id)
        message = f'Instance ID {instance_id} added successfully'

    return render_template('index.html', instance_ids=instance_ids, message=message)

@app.route('/perform_action', methods=['POST'])
def perform_action():
    instance_id = request.form.get('instance_id')
    action = request.form.get('action')

    access_key = os.environ.get('AWS_ACCESS_KEY_ID')
    secret_key = os.environ.get('AWS_SECRET_ACCESS_KEY')

    session = boto3.Session(
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name='eu-central-1'
    )
    ec2_client = session.client('ec2')

    if action == 'start':
        status = start_instance(ec2_client, instance_id)
        if status == 'running':
            message = f'EC2 instance {instance_id} is already running'
        else:
            message = f'Starting EC2 instance: {instance_id}'
    elif action == 'stop':
        stopped = stop_instance(ec2_client, instance_id)
        if stopped:
            message = f'EC2 instance {instance_id} has been stopped'
        else:
            message = f'EC2 instance {instance_id} is already stopped'
    elif action == 'info':
        instance_info = get_instance_info(ec2_client, instance_id)
        return render_template('instance_info.html', instance_info=instance_info)

    return render_template('result.html', message=message)

@app.route('/clear_instances', methods=['POST'])
def clear_instances():
    instance_ids.clear()
    return redirect('/')

def start_instance(client, instance_id):
    response = client.start_instances(InstanceIds=[instance_id])
    return response['StartingInstances'][0]['CurrentState']['Name']

def stop_instance(client, instance_id):
    response = client.stop_instances(InstanceIds=[instance_id])
    return response['StoppingInstances'][0]['CurrentState']['Name']

def get_instance_info(client, instance_id):
    response = client.describe_instances(InstanceIds=[instance_id])
    instance_info = {}

    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            instance_info['Instance ID'] = instance['InstanceId']
            instance_info['Instance Type'] = instance['InstanceType']
            instance_info['Public IP'] = instance.get('PublicIpAddress', 'N/A')
            instance_info['Private IP'] = instance['PrivateIpAddress']
            instance_info['State'] = instance['State']['Name']

    return instance_info

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
